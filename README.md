# WissKI Theme Content Types

This module adds additional content types which are most suited for the UB Heidelberg WissKI theme, but can also be used with other themes These additional content types are intended for the frontpage and are modeled after Bootstrap in their presentation style.

### Installing

Put the module folder into /modules/custom and activate it under /admin/modules.

